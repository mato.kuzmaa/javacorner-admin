package com.javacorner.admin.runner;

import com.javacorner.admin.entity.Course;
import com.javacorner.admin.entity.Instructor;
import com.javacorner.admin.entity.Student;
import com.javacorner.admin.entity.User;
import com.javacorner.admin.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyRunner implements CommandLineRunner {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private InstructorService instructorService;

    public static final String ADMIN = "Admin";
    public static final String INSTRUCTOR = "Instructor";
    public static final String STUDENT = "Student";

    public MyRunner() {
    }

    @Override
    public void run(String... args) throws Exception {
        User user1 = userService.createUser("user1@gmail.com", "pass1");
        User user2 = userService.createUser("user2@gmail.com", "pass2");

        roleService.createRole(ADMIN);
        roleService.createRole(INSTRUCTOR);
        roleService.createRole(STUDENT);

        userService.assignRoleToUser(user1.getEmail(), ADMIN);
        userService.assignRoleToUser(user2.getEmail(), ADMIN);

        Instructor instructor1 = instructorService.createInstructor("Marek", "Biely",
                "Experienced instructor", "instructorUser1@gmail.com", "pass1");
        Instructor instructor2 = instructorService.createInstructor("Peter", "Cierny",
                "Senior instructor", "instructorUser2@gmail.com", "pass2");

        Student student1 = studentService.createStudent("Lukas", "Brzobohaty", "beginner",
                "stdUser1@gmail.com", "pass1");
        Student student2 = studentService.createStudent("Stefan", "Lakatos", "master degree",
                "stdUser2@gmail.com", "pass2");
        Student student3 = studentService.createStudent("Filip", "Kratky", "master degree",
                "stdUser3@gmail.com", "pass3");

        Course course1 = courseService.createCourse("Spring Boot", "20 hours",
                "Master Spring service", instructor2.getInstructorId());
        Course course2 = courseService.createCourse("Spring Data JPA", "40 hours",
                "Introduction to JPA", instructor2.getInstructorId());
        Course course3 = courseService.createCourse("OOP principles", "30 hours",
                "OOP basics", instructor1.getInstructorId());
        Course course4 = courseService.createCourse("Java programming", "135 hours",
                "Basics of programming", instructor1.getInstructorId());

        courseService.assignStudentToCourse(course1.getCourseId(), student2.getStudentId());
        courseService.assignStudentToCourse(course2.getCourseId(), student3.getStudentId());
        courseService.assignStudentToCourse(course3.getCourseId(), student1.getStudentId());
        courseService.assignStudentToCourse(course3.getCourseId(), student2.getStudentId());
        courseService.assignStudentToCourse(course3.getCourseId(), student3.getStudentId());
        courseService.assignStudentToCourse(course4.getCourseId(), student3.getStudentId());

    }
}
