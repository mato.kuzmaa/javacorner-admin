package com.javacorner.admin.utility;

import com.javacorner.admin.repository.*;
import com.javacorner.admin.entity.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

/**
 * tato trieda mi sluzi len na otestovanie dat,
 * zamerane na vkladania udajov do tabuliek
 */

public class OperationUtility {

    public static void usersOperations(UserRepository userRepository) {
          createUser(userRepository);
//        updateUser(userDto);
//        deleteUser(userDto);
//        fetchUser(userDto);     // nacitat uzivatela
    }

    public static void rolesOperations(RoleRepository roleRepository) {
          createRole(roleRepository);
//        updateRole(roleDto);
//        deleteRole(roleDto);
//        fetchRole(roleDto);
    }

    public static void instructorsOperations(UserRepository userRepository, InstructorRepository instructorRepository, RoleRepository roleRepository) {
          createInstructor(userRepository, instructorRepository, roleRepository);
//        updateInstructor(instructorDto);
//        removeInstructor(instructorDto);
//        fetchInstructor(instructorDto);
    }

    public static void studentsOperations(UserRepository userRepository, StudentRepository studentRepository, RoleRepository roleRepository) {
          createStudent(userRepository, studentRepository, roleRepository);
        //updateStudent(studentDto);
        //removeStudent(studentDto);
        //fetchStudent(studentDto);
    }

    public static void coursesOperations(CourseRepository courseDto, InstructorRepository instructorRepository, StudentRepository studentRepository) {
        createCourse(courseDto, instructorRepository);
          //updateCourse(courseDto);
//        deleteCourse(courseDto);
//        fetchCourse(courseDto);
          assignStudentsToCourse(courseDto, studentRepository);
//        fetchCoursesForStudent(courseDto);
    }


    // methods for User
    private static void createUser(UserRepository userRepository) {
        User user1 = new User("user1@gmail.com", "pass1");
        userRepository.save(user1);
        User user2 = new User("user2@gmail.com", "pass2");
        userRepository.save(user2);
        User user3 = new User("user3@gmail.com", "pass3");
        userRepository.save(user3);
        User user4 = new User("user4@gmail.com", "pass4");
        userRepository.save(user4);
    }

    private static void updateUser(UserRepository userRepository) {
        User user = userRepository.findById(2L)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
        user.setEmail("newEmail@gmail.com");
        userRepository.save(user);
    }

    private static void deleteUser(UserRepository userRepository) {
        User user = userRepository.findById(3L)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
        userRepository.delete(user);
    }

    private static void fetchUser(UserRepository userRepository) {
        userRepository.findAll().forEach(user -> System.out.println(user.toString()));
    }


    //methods for Role
    private static void createRole(RoleRepository roleRepository) {
        Role role1 = new Role("Admin");
        roleRepository.save(role1);
        Role role2 = new Role("Instructor");
        roleRepository.save(role2);
        Role role3 = new Role("Student");
        roleRepository.save(role3);
    }

    private static void updateRole(RoleRepository roleRepository) {
        Role role = roleRepository.findById(1L)
                .orElseThrow(()-> new EntityNotFoundException("Role not found"));
        role.setName("NewAdmin");
        roleRepository.save(role);
    }

    private static void deleteRole(RoleRepository roleRepository) {
        roleRepository.deleteById(2L);
    }

    private static void fetchRole(RoleRepository roleRepository) {
        roleRepository.findAll().forEach(role -> System.out.println(role.toString()));
    }

    public static void assignRolesToUsers(UserRepository userRepository, RoleRepository roleRepository) {
        Role role = roleRepository.findByName("Admin");
        if(role==null) throw new EntityNotFoundException("Role not found");
        List<User> users = userRepository.findAll();
        users.forEach(user -> {
            user.assignRoleToUser(role);
            userRepository.save(user);
        });
    }

    // methods for instructors

    private static void createInstructor(UserRepository userRepository, InstructorRepository instructorRepository, RoleRepository roleRepository) {
        Role role = roleRepository.findByName("Instructor");
        if(role==null) throw new EntityNotFoundException("Role not found");

        User user1 = new User("instructorUser1@gmail.com", "pass1");
        user1.assignRoleToUser(role);
        userRepository.save(user1);
        Instructor instructor1 = new Instructor("instructor1FN", "instructor1LN", "Experienced Instructor", user1);
        instructorRepository.save(instructor1);

        User user2 = new User("instructorUser2@gmail.com", "pass2");
        user2.assignRoleToUser(role);
        userRepository.save(user2);
        Instructor instructor2 = new Instructor("instructor2FN", "instructor2LN", "Senior Instructor", user2);
        instructorRepository.save(instructor2);
    }

    private static void updateInstructor(InstructorRepository instructorRepository) {
        Instructor instructor = instructorRepository.findById(1L)
                .orElseThrow(() -> new EntityNotFoundException("Instructor not found"));
        instructor.setSummary("Certified Instructor");
        instructorRepository.save(instructor);
    }

    private static void removeInstructor(InstructorRepository instructorRepository) {
        instructorRepository.deleteById(2L);
    }

    private static void fetchInstructor(InstructorRepository instructorRepository) {
        instructorRepository.findAll().forEach(instructor -> System.out.println(instructor.toString()));
    }

    // methods for students

    private static void createStudent(UserRepository userRepository, StudentRepository studentRepository, RoleRepository roleRepository) {
        Role role = roleRepository.findByName("Student");
        if(role == null) throw new EntityNotFoundException("Role not found");

        User user1 = new User("stdUser1@gmail.com", "pass1");
        user1.assignRoleToUser(role);
        userRepository.save(user1);
        Student student1 = new Student("student1FN", "student1LN", "master", user1);    // FN -first name, LN - last name
        studentRepository.save(student1);

        User user2 = new User("stdUser2@gmail.com", "pass2");
        user2.assignRoleToUser(role);
        userRepository.save(user2);
        Student student2 = new Student("student2FN", "student2LN", "Phd", user2);
        studentRepository.save(student2);
    }

    private static void updateStudent(StudentRepository studentRepository) {
        Student student = studentRepository.findById(2L)
                .orElseThrow(() -> new EntityNotFoundException("Student not found"));
        student.setFirstName("updatedStdFN");
        student.setLastName("updatedStdLN");
        studentRepository.save(student);
    }

    private static void removeStudent(StudentRepository studentRepository) {
        studentRepository.deleteById(1L);
    }

    private static void fetchStudent(StudentRepository studentRepository) {
        studentRepository.findAll().forEach(student -> System.out.println(student.toString()));
    }

    // methods for courses

    private static void createCourse(CourseRepository courseDto, InstructorRepository instructorRepository) {
        Instructor instructor = instructorRepository.findById(1L)
                .orElseThrow(() -> new EntityNotFoundException("Instructor not found"));

        Course course1 = new Course("Hibernate", "5 hours", "Introduction to Hibernate", instructor);
        courseDto.save(course1);
        Course course2 = new Course("Spring data JPA", "10 hours", "Master Spring data JPA", instructor);
        courseDto.save(course2);
    }

    private static void updateCourse(CourseRepository courseDto) {
        Course course = courseDto.findById(1L)
                .orElseThrow(() -> new EntityNotFoundException("Course not found"));
        course.setCourseDuration("20 hours");
        courseDto.save(course);
    }

    private static void deleteCourse(CourseRepository courseDto) {
        courseDto.deleteById(2L);
    }

    private static void fetchCourse(CourseRepository courseDto) {
        courseDto.findAll().forEach(course -> System.out.println(course.toString()));
    }

    private static void assignStudentsToCourse(CourseRepository courseDto, StudentRepository studentRepository) {
        Optional<Student> student1 = studentRepository.findById(1L);
        Optional<Student> student2 = studentRepository.findById(2L);
        Course course = courseDto.findById(1L)
                .orElseThrow(() -> new EntityNotFoundException("Course not found"));

        student1.ifPresent(course::assignStudentToCourse);
        student2.ifPresent(course::assignStudentToCourse);
        courseDto.save(course);
    }

    private static void fetchCoursesForStudent(CourseRepository courseDto) {
        courseDto.getCoursesByStudentId(1L).forEach(course -> System.out.println(course.toString()));
    }

}


























