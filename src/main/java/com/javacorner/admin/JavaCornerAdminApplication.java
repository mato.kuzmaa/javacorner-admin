package com.javacorner.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class JavaCornerAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaCornerAdminApplication.class, args);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}

// zakomentovane riadky:
//        maju byt umiestnene v ramci triedy JavaCornerAdminApplication
//        nimi som testoval metody v ramci triedy OperationUtility
//        nepotrebujem to uz aktualne, ani triedu, ani tieto riadky
//
//    @Override
//    public void run(String... args) throws Exception {
//        OperationUtility.usersOperations(userDto);
//        OperationUtility.rolesOperations(roleDto);
//        OperationUtility.assignRolesToUsers(userDto, roleDto);
//        OperationUtility.instructorsOperations(userDto,instructorDto,roleDto);
//        OperationUtility.studentsOperations(userDto, studentDto, roleDto);
//        OperationUtility.coursesOperations(courseDto, instructorDto, studentDto);
//        }
