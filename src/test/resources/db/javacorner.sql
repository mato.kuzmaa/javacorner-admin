INSERT INTO `courses` VALUES
(1,'Master Spring service','2 hours','Spring Service',1),
(2,'Introduction to JPA','4 hours','Spring Data JPA',2);

INSERT INTO `enrolled_in` VALUES
(1,1),
(2,2);

INSERT INTO `instructors` VALUES
(1,'instructor1FN','instructor1LN','Experienced Instructor',3),
(2,'instructor2FN','instructor2LN','Senior Instructor',4);

INSERT INTO `roles` VALUES
(1,'Admin'),
(2,'Instructor'),
(3,'Student');

INSERT INTO `students` VALUES
(1,'std1FN','std1LN','beginner',5),
(2,'std2FN','std2LN','master degree',6);

INSERT INTO `user_role` VALUES
(1,1),
(1,2),
(1,3),
(3,2),
(4,2),
(5,3),
(6,3);

INSERT INTO `users` VALUES
(1,'user1@gmail.com','pass1'),(2,'user2@gmail.com','pass2'),
(3,'instructorUser1@gmail.com','pass1'),(4,'instructorUser2@gmail.com','pass2'),
(5,'stdUser1@gmail.com','pass1'),(6,'stdUser2@gmail.com','pass2');
