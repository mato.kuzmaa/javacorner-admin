package com.javacorner.admin.service.impl;

import com.javacorner.admin.repository.CourseRepository;
import com.javacorner.admin.repository.InstructorRepository;
import com.javacorner.admin.repository.StudentRepository;
import com.javacorner.admin.entity.Course;
import com.javacorner.admin.entity.Instructor;
import com.javacorner.admin.entity.Student;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CourseServiceImplTest {

    @Mock
    private CourseRepository courseDto;

    @Mock
    private InstructorRepository instructorRepository;

    @Mock
    private StudentRepository studentRepository;

    @InjectMocks
    private CourseServiceImpl courseService;

    @Test
    void testLoadCourseById() {
        Course course = new Course();
        course.setCourseId(1L);

        when(courseDto.findById(any())).thenReturn(Optional.of(course));

        Course actualCourse = courseService.loadCourseById(1L);

        assertEquals(course, actualCourse);
    }

    @Test
    void testExceptionForNotFoundCourseById() {
        assertThrows(EntityNotFoundException.class, ()-> courseService.loadCourseById(2L));
    }

    @Test
    void testCreateCourse() {
        Instructor instructor = new Instructor();
        instructor.setInstructorId(1L);

        when(instructorRepository.findById(any())).thenReturn(Optional.of(instructor));

        courseService.createCourse("JPA", "1h 30 min", "Hello JPA", 1L);
        verify(courseDto).save(any());
    }

    @Test
    void testCreateOrUpdateCourse() {
        Course course = new Course();
        course.setCourseId(1L);

        courseService.createOrUpdateCourse(course);

        ArgumentCaptor<Course> argumentCaptor = ArgumentCaptor.forClass(Course.class);
        verify(courseDto).save(argumentCaptor.capture());

        Course capturedValue = argumentCaptor.getValue();

        assertEquals(course, capturedValue);
    }

    @Test
    void testFindCoursesByCourseName() {
        String courseName = "JPA";
        courseService.findCoursesByCourseName(courseName);

        verify(courseDto).findCourseByCourseNameContains(courseName);
    }

    @Test
    void testAssignStudentToCourse() {
        Student student = new Student();
        student.setStudentId(2L);

        Course course = new Course();
        course.setCourseId(1L);

        when(studentRepository.findById(any())).thenReturn(Optional.of(student));
        when(courseDto.findById(any())).thenReturn(Optional.of(course));

        courseService.assignStudentToCourse(1L, 1l);
    }

    @Test
    void testFetchAll() {
        courseService.fetchAll();
        verify(courseDto).findAll();
    }

    @Test
    void testFetchCoursesForStudent() {
        courseService.fetchCoursesForStudent(1L);
        verify(courseDto).getCoursesByStudentId(1L);
    }

    @Test
    void testRemoveCourse() {
        courseService.removeCourse(1L);
        verify(courseDto).deleteById(1L);
    }
}













