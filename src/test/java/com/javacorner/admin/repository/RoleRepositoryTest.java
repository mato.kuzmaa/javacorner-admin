package com.javacorner.admin.repository;

import com.javacorner.admin.AbstractTest;
import com.javacorner.admin.entity.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"file:src/test/resources/db/clearAll.sql", "file:src/test/resources/db/javacorner.sql"})
class RoleRepositoryTest extends AbstractTest {

    @Autowired
    private RoleRepository roleRepository;

    @Test
    void testFindByName() {
        String roleName = "Admin";
        Role role = roleRepository.findByName(roleName);
        assertEquals(roleName, role.getName());
    }

    @Test
    void testFindNonExistingRole() {
        String roleName = "NewRole";
        Role role = roleRepository.findByName(roleName);
        assertNull(role);
    }
}