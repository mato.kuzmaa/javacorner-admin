package com.javacorner.admin.repository;

import com.javacorner.admin.AbstractTest;
import com.javacorner.admin.entity.Instructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"file:src/test/resources/db/clearAll.sql", "file:src/test/resources/db/javacorner.sql"})
class InstructorRepositoryTest extends AbstractTest {

    @Autowired
    private InstructorRepository instructorRepository;

    @Test
    void testFindInstructorsByName() {
        List<Instructor> instructors = instructorRepository.findInstructorsByName("instructor1FN");
        int exceptedValue = 1;
        assertEquals(exceptedValue, instructors.size());
    }

    @Test
    void testFindInstructorByEmail() {
        Instructor expectedInstructor = new Instructor();
        expectedInstructor.setInstructorId(1L);
        expectedInstructor.setFirstName("instructor1FN");
        expectedInstructor.setLastName("instructor1LN");
        expectedInstructor.setSummary("Experienced Instructor");

        Instructor instructor = instructorRepository.findInstructorByEmail("instructorUser1@gmail.com");

        assertEquals(expectedInstructor, instructor);
    }

    @Test
    void testFindInstructorByNotExistingEmail() {
        Instructor instructor = instructorRepository.findInstructorByEmail("javacorner@gmail.com");
        assertNull(instructor);
    }
}